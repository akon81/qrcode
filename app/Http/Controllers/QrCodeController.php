<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QrCodeController extends Controller
{
    public function show()
    {
        $a=0;
        $file='flat';

        $contents = Storage::get('public/plate_'.$file.'.csv');
        $base = explode("\n", $contents);

        foreach($base as $b)
        {
            if(!empty($b)){ 
                $a++;
                $number=str_pad($a,3,'0', STR_PAD_LEFT);
                $kod=trim(substr($b,28));
                $data = QrCode::size(32)
                ->format('svg')
                ->backgroundColor(255, 0, 0)
                ->color(255, 255, 255)
                ->margin(1)
                ->errorCorrection('L')
                ->generate(
                    $b,
            );
            $path=$file.'_'.$number.'_'.$kod.'.svg';
            Storage::put('public/'.$path, $data);
        }
        }
        return  $a;
    }
}